import requests
import uvicorn
from fastapi import FastAPI
from typing import Optional
from pydantic import BaseModel


app = FastAPI()


@app.get("/")
async def root():
    return "Examen conception logicielle"


@app.get("/creer-un-deck/")
async def create_deck():
    r = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    if r.status_code != 200:
        return ("Error while requesting https://deckofcardsapi.com/ or error: " + str(r.status_code))
    else:
        return r.json()["deck_id"]


@app.post("/cartes/{nombres_cartes}/{deck_id}")
async def draw_cards(deck_id: str, nombres_cartes: int):
    r = requests.get("https://deckofcardsapi.com/api/deck/{}".format(deck_id) + "/draw/?count={}".format(nombres_cartes))
    if r.status_code != 200:
        return ("Error: " + str(r.status_code))
    else:
        return r.json()["cards"]

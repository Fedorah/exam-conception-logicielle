import requests


def main(run):
    while run:
        print("Welcome")
        print("Do you want to get a new card deck ? (y/n)")
        answer = input()
        if answer == "n": 
            break
        elif answer == "y":
            deck_id = requests.get("http://127.0.0.1:8000/creer-un-deck").json()
            print("Your deck id is: {}".format(deck_id))
            print("By default your deck is shuffled.")
        print("Do you want to draw some cards ? (y/n)")
        answer = input()
        if answer == "n":
            break
        elif answer == "y":
            print("How much ? (integer between 1 and 52)")
            number_of_cards = input()
            list_of_cards = requests.post("http://127.0.0.1:8000/cartes/{}/{}".format(number_of_cards, deck_id)).json()
            print("This is your cards: \n {}".format(list_of_cards))
        print("Now you can count how much of cards for every suits in your card selection (y/n)")
        answer = input()
        if answer == "n":
            break
        elif answer == "y":
            dict_of_cards_by_number = get_number_of_cards_by_suit(list_of_cards)
            print("This is the number by suit of your cards : \n {}".format(dict_of_cards_by_number))
        run = False
    print("This small demo client terminates here.")


def get_number_of_cards_by_suit(card_list):
    res = {"H": 0, "S": 0, "D": 0, "C": 0}
    for card in card_list:
        if card["suit"] == "CLUBS":
            res["C"] += 1
        elif card["suit"] == "DIAMONDS":
            res["D"] += 1
        elif card["suit"] == "HEART":
            res["H"] += 1
        else:
            res["S"] += 1
    return res

if __name__ == "__main__":
    run = True
    main(run)
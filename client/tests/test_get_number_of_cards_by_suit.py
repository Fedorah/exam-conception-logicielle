import requests

from ..main import get_number_of_cards_by_suit


class TestClientFunctionalities:
    def test_get_number_of_cards_by_suit(sefl):
        # SETUP
        deck_id = requests.get("https://deckofcardsapi.com/api/deck/new/").json()["deck_id"]
        number_of_cards = 10
        list_of_cards = requests.post("http://127.0.0.1:8000/cartes/{}/{}".format(number_of_cards, deck_id)).json()
        # GIVEN
        dict_of_cards_by_number = get_number_of_cards_by_suit(list_of_cards)
        expected_result = {'H': 0, 'S': 10, 'D': 0, 'C': 0}
        # THEN
        assert expected_result == dict_of_cards_by_number

# Examen Conception Logicielle

## A propos

Cet examen se présente sous la forme d'un mini-projet dont l'objectif est de créer un webservice et un client en Python tout en respectant les principes de conception logicielle vus en cours.

Actuellement, mon projet comporte un petit webservice supporté par FastAPI et un client minimaliste en ligne de commande.

Le webservice implémente une petite API contenant les endpoints suivants :

```
/creer-un-deck/
/cartes/{nombre_de_cartes}/{deck_id}
/cartes/
```

Le webservice utilise l'API publique https://deckofcardsapi.com/.

Le premier endpoint renvoie l'identifiant sous forme de chaîne de caractère d'un nouveau deck de 52 cartes mélangé.

Le second endpoint propose de piocher un certain nombre de cartes dans le nouveau deck.

Le dernier endpoint calcule le nombre de carte par famille (trèfles, carreaux, coeurs et piques) parmis la sélection de cartes obtenus via le deuxième endpoint.

## Installation et démarage rapide

Cloner le dépôt et se placer à la racine du projet :

```
git clone https://gitlab.com/Fedorah/exam-conception-logicielle.git
cd examen-conception-logiciel
```

Le projet a deux composants : le client et le webservice.

### Webservice

#### Installation des dépendances

```
cd webservice
pip install -r requirements.txt
```

#### Lancement du serveur

```
uvicorn main:app --reload
```

### Client

#### Installation des dépendances

A la racine du projet:

```
cd client
pip install -r requirements.txt
```

#### Lancement du client

```
python main.py
```

### Client PyInquirer

Se placer sur la branche better_client_dev depuis master

```
git checkout better_client_dev
```

Installer les dépendances

```
cd client
pip install -r requirements.txt
```

Lancer le server comme pour la branche master et :

```
python inquirer.py
```

Il reste des bugs et des améliorations de l'interface à implémenter.
L'architecture des menus n'est pas optimale.

## Tests

Actuellement il n'y a qu'un seul test unitaire permettant de tester la fonctionnalité de comptage des cartes.
Pour lancer le test se placer à la racine du projet et faire :

```
pytest
```

## WIP

TODO

1. Documentation du code
2. Fixer les bugs du client avec PyInquirer et remanier l'architecture des menus
3. Meilleur affichage de certaines sorties du client avec PyInquirer
4. Fixer l'histoire de la branche better_client_dev avec l'histoire de master
5. Merge better_client_dev avec master
6. Fixer l'intégration continue pour les tests
7. Intégration continue pour le linting
